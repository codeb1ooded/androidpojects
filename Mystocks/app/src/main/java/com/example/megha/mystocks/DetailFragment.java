package com.example.megha.mystocks;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Megha on 24-03-2016.
 */
public class DetailFragment extends Fragment {

    TextView name, price, ticker, change;
    Stocks myStock;
    public static String selectedStock = "selected_stock";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.detail_stock_fragment , container, false);
        name = (TextView) v.findViewById(R.id.name);
        price = (TextView) v.findViewById(R.id.currentprice);
        ticker = (TextView) v.findViewById(R.id.ticker);
        change = (TextView) v.findViewById(R.id.percentageChange);

        Bundle b = getArguments();
        if(b != null){
            myStock = (Stocks) b.getSerializable(selectedStock);
            if(myStock != null){
                name.setText(myStock.getCompanyName());
                price.setText(myStock.getCurrentPrice()+"");
                ticker.setText(myStock.getTicker());
                change.setText(myStock.getPercentChange()+"");
            }
        }
        return v;
    }

    void setStocks(Stocks myStock){
        name.setText(myStock.getCompanyName());
        price.setText(myStock.getCurrentPrice()+"");
        ticker.setText(myStock.getTicker());
        change.setText(myStock.getPercentChange()+"");
    }
}
