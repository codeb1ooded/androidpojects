package com.example.megha.mystocks;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Megha on 23-03-2016.
 */
public class StocksListAdapter extends ArrayAdapter<Stocks> {

    Context context;
    ArrayList<Stocks> myStocks;
    public StocksListAdapter(Context context, ArrayList<Stocks> objects) {
        super(context, 0, objects);
        myStocks = objects;
        this.context = context;
    }

    static class ViewHolder{
        TextView companyNameTextView;
        TextView percentageChangeTextView;
        TextView priceTextView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = View.inflate(context, R.layout.stock_single_item_layout, null);
            ViewHolder vh = new ViewHolder();
            vh.companyNameTextView = (TextView) convertView.findViewById(R.id.companyNameFromListFragment);
            vh.percentageChangeTextView = (TextView) convertView.findViewById(R.id.percentageChangeFromListFragment);
            vh.priceTextView = (TextView) convertView.findViewById(R.id.currentPriceFromListFragment);
            convertView.setTag(vh);
        }
        Stocks currentStock = myStocks.get(position);
        ViewHolder vh = (ViewHolder) convertView.getTag();
        vh.companyNameTextView.setText(currentStock.getCompanyName());
        vh.priceTextView.setText(currentStock.getCurrentPrice()+"");
        vh.percentageChangeTextView.setText((currentStock.getPercentChange()+""));
        return convertView;
    }
}
