package com.example.megha.mystocks;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class StocksListFragment extends Fragment implements StocksListExtractAsyncTask.StocksAsyncTaskInterface {

    ListView lv;
    stocksListFragmentInterface listener;
    ArrayList<Stocks> myStocks;
    StocksListAdapter adapter;

    @Override
    public void onTaskComplete(ArrayList<Stocks> stocks) {
        if(stocks != null)
        for(int i = 0; i < stocks.size(); i++){
            this.myStocks.add(stocks.get(i));
        }
        adapter.notifyDataSetChanged();
    }

    interface stocksListFragmentInterface{
        void onItemClicked(Stocks s);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.stocks_list_fragment, container, false);
        myStocks =  new ArrayList<>();
        lv = (ListView) view.findViewById(R.id.stocksListViewFragment);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Stocks s = myStocks.get(position);
                listener = (stocksListFragmentInterface) getActivity();
                listener.onItemClicked(s);
            }
        });
        StocksListExtractAsyncTask asyncTask = new StocksListExtractAsyncTask(this);
        asyncTask.execute(getUrl());
        myStocks = new ArrayList<>();
        adapter = new StocksListAdapter(getActivity(), myStocks);
        lv.setAdapter(adapter);
        return view;
    }


    private String getUrl() {
        return "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22YHOO%22%2C%22GOOG%22%2C%22MSFT%22%2C%22AMZN%22)%0A%09%09&env=http%3A%2F%2Fdatatables.org%2Falltables.env&format=json";
    }
}
