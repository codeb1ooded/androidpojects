package com.example.megha.mystocks;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Megha on 23-03-2016.
 */
public class StocksListExtractAsyncTask extends AsyncTask<String, Void, ArrayList<Stocks>> {

    @Override
    protected ArrayList<Stocks> doInBackground(String... params) {
        String urlString = params[0];
        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();
            InputStream inputStream = urlConnection.getInputStream();
            if (inputStream == null) {
                return null;
            }
            Scanner s = new Scanner(inputStream);
            StringBuffer output = new StringBuffer();
            while (s.hasNext()) {
                output.append(s.nextLine());
            }
            return parseStocksJSON(output.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private ArrayList<Stocks> parseStocksJSON(String output) {
        try {
            JSONObject obj = new JSONObject(output);
            JSONObject query = obj.getJSONObject("query");
            JSONObject results = query.getJSONObject("results");
            JSONArray quotes = results.getJSONArray("quote");
            ArrayList<Stocks> myStocks = new ArrayList<>();

            Log.v("Stocks length", quotes.length()+"========================================================================");
            for(int i = 0; i < quotes.length(); i++){
                JSONObject currentStock = quotes.getJSONObject(i);
                Stocks s = new Stocks();
                s.setCompanyName(currentStock.getString("Name"));
                s.setTicker(currentStock.getString("symbol"));
                s.setCurrentPrice(Double.parseDouble(currentStock.getString("Bid")));
                s.setPercentChange(Double.parseDouble(currentStock.getString("Change")));
                myStocks.add(s);
            }
            return myStocks;

        } catch (JSONException e) {
            return null;
        }

    }

    public interface StocksAsyncTaskInterface{
        void onTaskComplete(ArrayList<Stocks> stocks);
    }

    StocksAsyncTaskInterface listener;
    public StocksListExtractAsyncTask(StocksAsyncTaskInterface listener){
        this.listener = listener;
    }

    @Override
    protected void onPostExecute(ArrayList<Stocks> stocks) {
        if(listener != null){
            listener.onTaskComplete(stocks);
        }
    }

}
