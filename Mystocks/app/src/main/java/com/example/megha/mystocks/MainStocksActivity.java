package com.example.megha.mystocks;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

import java.io.Serializable;

public class MainStocksActivity extends AppCompatActivity implements StocksListFragment.stocksListFragmentInterface {

    FrameLayout fl;
    public static String portraitStocks;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_stocks);
    }

    @Override
    public void onItemClicked(Stocks s) {
        fl = (FrameLayout) findViewById(R.id.landscapeFrameLayout);
        if(fl == null){
            //Portrait View
            Intent i = new Intent();
            i.setClass(this, DetailActivityPortrait.class);
            i.putExtra(portraitStocks, s);
            startActivity(i);
        }
        else{
            //lanscape View
            DetailFragment df = new DetailFragment();
            Bundle b = new Bundle();
            b.putSerializable(DetailFragment.selectedStock, s);
            df.setArguments(b);
            getFragmentManager().beginTransaction().replace(R.id.landscapeFrameLayout, df).commit();
        }
    }
}
