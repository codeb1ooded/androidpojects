package com.example.megha.mystocks;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class DetailActivityPortrait extends AppCompatActivity {

    Intent i;
    Stocks s;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_activity_portrait);
        i = getIntent();
        DetailFragment df =
                (DetailFragment)getFragmentManager().findFragmentById(R.id.detailPortraitFragment);
        s = (Stocks) i.getSerializableExtra(MainStocksActivity.portraitStocks);
        df.setStocks(s);
    }
}
